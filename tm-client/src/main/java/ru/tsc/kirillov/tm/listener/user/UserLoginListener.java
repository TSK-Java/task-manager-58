package ru.tsc.kirillov.tm.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.UserLoginRequest;
import ru.tsc.kirillov.tm.dto.response.UserLoginResponse;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class UserLoginListener extends AbstractUserListener {

    @NotNull
    @Override
    public String getName() {
        return "login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Выполнение авторизации.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@userLoginListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Авторизация]");
        System.out.println("Введите логин");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Введите пароль");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        @NotNull final UserLoginResponse response = getAuthEndpoint().login(request);
        setToken(response.getToken());
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
