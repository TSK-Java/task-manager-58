package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.SessionDTO;
import ru.tsc.kirillov.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    String login (@Nullable String login, @Nullable String password);

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
